%define nightly %{nil}
%define _sysconfdir %{_prefix}/etc
%define release 1.1
%define configure_opts %{nil}

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

# -- metadata ---------------

Name: lalburst
Version: 2.0.3
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
License: GPLv2+
Source0: https://software.igwn.org/lscsoft/source/lalsuite/%{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/Computing/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
Prefix: %{_prefix}

# -- build requirements -----

# C
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: gsl-devel
BuildRequires: help2man >= 1.37
BuildRequires: liblal-devel >= 7.4.0
BuildRequires: liblalmetaio-devel >= 4.0.0
BuildRequires: liblalsimulation-devel >= 5.3.0
BuildRequires: make
BuildRequires: pkgconfig >= 0.18.0

# swig
BuildRequires: swig >= 3.0.11

# python3x
BuildRequires: epel-rpm-macros
BuildRequires: python-srpm-macros
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}-lal >= 7.4.0
BuildRequires: python%{python3_pkgversion}-lalmetaio >= 4.0.0
BuildRequires: python%{python3_pkgversion}-lalsimulation >= 5.3.0
BuildRequires: python%{python3_pkgversion}-ligo-lw >= 1.7.0
BuildRequires: python%{python3_pkgversion}-lscsoft-glue
BuildRequires: python%{python3_pkgversion}-matplotlib
BuildRequires: python%{python3_pkgversion}-numpy >= 1.7
BuildRequires: python%{python3_pkgversion}-pillow
BuildRequires: python%{python3_pkgversion}-pytest
BuildRequires: python%{python3_pkgversion}-scipy
BuildRequires: python3-rpm-macros

# octave
BuildRequires: lal-octave >= 7.4.0
BuildRequires: lalmetaio-octave >= 4.0.0
BuildRequires: lalsimulation-octave >= 5.3.0
BuildRequires: octave-devel

# -- packages ---------------

# lalburst
Summary: LSC Algorithm Burst Libary - runtime tools
Requires: lib%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}-lal
Requires: python%{python3_pkgversion}-%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}-lalmetaio
Requires: python%{python3_pkgversion}-lalsimulation
Requires: python%{python3_pkgversion}-ligo-lw
Requires: python%{python3_pkgversion}-ligo-segments
Requires: python%{python3_pkgversion}-matplotlib
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-pillow
Requires: python%{python3_pkgversion}-scipy
Requires: python%{python3_pkgversion}-tqdm
Obsoletes: python2-%{name} <= 1.5.8-1
%description
The LSC Algorithm Burst Library for gravitational wave data analysis.
This package provides the runtime tools.

%package -n lib%{name}
Summary: LSC Algorithm Burst Library - library package
Requires: liblal >= 7.4.0
Requires: liblalmetaio >= 4.0.0
Requires: liblalsimulation >= 5.3.0
Conflicts: %{name} <= 1.5.6-1
%description -n lib%{name}
The LSC Algorithm Burst Library for gravitational wave data analysis.
This package contains the shared-object libraries needed to run applications
that use the LAL Burst library.

# liblalburst-devel
%package -n lib%{name}-devel
Summary: Files and documentation needed for compiling programs that use LAL Burst
Requires: gsl-devel
Requires: lib%{name} = %{version}-%{release}
Requires: liblal-devel >= 7.4.0
Requires: liblalmetaio-devel >= 4.0.0
Requires: liblalsimulation-devel >= 5.3.0
Provides: %{name}-devel = %{version}-%{release}
Obsoletes: %{name}-devel < 1.5.7-1
%description -n lib%{name}-devel
The LSC Algorithm Burst Library for gravitational wave data analysis.
This package contains files needed build applications that use the LAL Burst
library.

# python3x-lalburst
%package -n python%{python3_pkgversion}-%{name}
Summary: Python %{python3_version} Bindings for LALBurst
Requires: lib%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}
Requires: python%{python3_pkgversion}-lal >= 7.4.0
Requires: python%{python3_pkgversion}-lalmetaio >= 4.0.0
Requires: python%{python3_pkgversion}-lalsimulation >= 5.3.0
Requires: python%{python3_pkgversion}-ligo-lw
Requires: python%{python3_pkgversion}-ligo-segments
Requires: python%{python3_pkgversion}-lscsoft-glue
Requires: python%{python3_pkgversion}-numpy >= 1.7
Requires: python%{python3_pkgversion}-scipy
Requires: python%{python3_pkgversion}-tqdm
Obsoletes: python2-%{name} < 1.5.9-1
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Python %{python3_version} bindings for LALBurst.

# lalburst-octave
%package octave
Summary: Octave Bindings for LALBurst
Requires: lib%{name} = %{version}-%{release}
Requires: octave
Requires: lal-octave >= 7.4.0
Requires: lalmetaio-octave >= 4.0.0
Requires: lalsimulation-octave >= 5.3.0
%description octave
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Octave bindings for LALBurst.

# -- build-stages -----------

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
%configure %{configure_opts} --disable-gcc-flags --enable-swig PYTHON=%{__python3}
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' -delete

%post -n lib%{name} -p /sbin/ldconfig

%postun -n lib%{name} -p /sbin/ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

# -- files ------------------

%files -n lib%{name}
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_libdir}/*.so.*

%files -n lib%{name}-devel
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_includedir}/lal
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files -n python%{python3_pkgversion}-%{name}
%defattr(-,root,root)
%doc README.md
%license COPYING
%{python3_sitearch}/*
%exclude %{python3_sitearch}/%{name}/*.a

%files octave
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_prefix}/lib*/octave/*/site/oct/*/lalburst.oct*

%files
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_bindir}/*
%{_mandir}/man1/*
%{_sysconfdir}/*

# -- changelog --------------

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Jan 11 2024 Adam Mercer <adam.mercer@ligo.org> 2.0.3-1.1
- Replace python-glue dependency with python-lscsoft-glue

* Fri Oct 20 2023 Adam Mercer <adam.mercer@ligo.org> 2.0.3-1
- Update for 2.0.3

* Fri Oct 06 2023 Adam Mercer <adam.mercer@ligo.org> 2.0.2-1
- Update for 2.0.2

* Thu Apr 06 2023 Adam Mercer <adam.mercer@ligo.org> 2.0.1-1
- Update for 2.0.1

* Mon Feb 06 2023 Adam Mercer <adam.mercer@ligo.org> 2.0.0-1
- Update for 2.0.0

* Thu Nov 03 2022 Adam Mercer <adam.mercer@ligo.org> 1.7.0-1
- Update for 1.7.0

* Mon Sep 05 2022 Adam Mercer <adam.mercer@ligo.org> 1.6.2-1
- Update for 1.6.2

* Thu Aug 18 2022 Adam Mercer <adam.mercer@ligo.org> 1.6.1-1
- Update for 1.6.1

* Tue Aug 02 2022 Adam Mercer <adam.mercer@ligo.org> 1.6.0-1
- Update for 1.6.0

* Thu Mar 03 2022 Adam Mercer <adam.mercer@ligo.org> 1.5.12-1
- Update for 1.5.12

* Mon Jan 10 2022 Adam Mercer <adam.mercer@ligo.org> 1.5.11-1
- Update for 1.5.11

* Fri Dec 03 2021 Adam Mercer <adam.mercer@ligo.org> 1.5.10-1
- Update for 1.5.10

* Mon May 17 2021 Adam Mercer <adam.mercer@ligo.org> 1.5.9-1
- Update for 1.5.9

* Fri Feb 05 2021 Adam Mercer <adam.mercer@ligo.org> 1.5.8-1
- Update for 1.5.8

* Mon Jan 11 2021 Adam Mercer <adam.mercer@ligo.org> 1.5.7-1
- Update for 1.5.7

* Wed Oct 28 2020 Adam Mercer <adam.mercer@ligo.org> 1.5.6-1
- Update for 1.5.6

* Mon Jun 08 2020 Adam Mercer <adam.mercer@ligo.org> 1.5.5-1
- Update for 1.5.5

* Fri Dec 20 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.4-1
- snglcoinc: fix rate_factors for more than two detector cases

* Mon Dec 09 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.3-3
- Packaging updates

* Thu Dec 05 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.3-2
- Packaging updates

* Mon Nov 25 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.3-1
- O3b release

* Thu May 23 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.2-1
- O3 release

* Mon Feb 25 2019 Adam Mercer <adam.mercer@ligo.org> 1.5.1-1
- ER14 release

* Thu Sep 13 2018 Adam Mercer <adam.mercer@ligo.org> 1.5.0-1
- Pre O3 release

* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 1.4.4-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.4.3-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 1.4.2-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 1.4.1-1
- Pre O2 packaging test release
